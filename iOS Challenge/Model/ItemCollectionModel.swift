//
//  ItemCollectionModel.swift
//  iOS Challenge
//
//  Created by Charles Black on 2/6/19.
//  Copyright © 2019 challenge. All rights reserved.
//

import UIKit

class ItemCollectionModel {
    
    var uuid: String
    var itemID: String
    var name: String
    var imageURL: String
    var linioPlusLevel: Int
    var conditionType: String
    var freeShipping: Bool
    var isImported: Bool
    var isActie: Bool
    
    init(uuid: String, itemID: String, name: String, imageURL: String, linioPlusLevel: Int, conditionType: String, freeShipping: Bool, isImported: Bool, isActie: Bool) {
        
        self.uuid = uuid
        self.itemID         = itemID
        self.name           = name
        self.imageURL       = imageURL
        self.linioPlusLevel = linioPlusLevel
        self.conditionType  = conditionType
        self.freeShipping   = freeShipping
        self.isImported     = isImported
        self.isActie        = isActie
        
    }
    
    
    
}
