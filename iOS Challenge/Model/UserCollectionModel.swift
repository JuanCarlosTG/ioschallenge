//
//  UserCollectionModel.swift
//  iOS Challenge
//
//  Created by Charles Black on 2/6/19.
//  Copyright © 2019 challenge. All rights reserved.
//

import UIKit

class UserCollectionModel {
    
    var nameCollection: String
    var products: [ItemCollectionModel] = []
    
    init(nameCollection: String) {
        
        self.nameCollection = nameCollection
        
    }
    
    
    
}
