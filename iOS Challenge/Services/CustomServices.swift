//
//  CustomServices.swift
//  iOS Challenge
//
//  Created by Charles Black on 2/6/19.
//  Copyright © 2019 challenge. All rights reserved.
//

import UIKit

class CustomServices: NSObject {
    
    typealias CompletionBlock = (_ data:Any?, _ completed:Bool) -> Void
    
    static func call(ws:String, completion:CompletionBlock?) {
        
        let session  = URLSession.shared
        let request  = URLRequest(url: URL(string: ws)!)
        
        session.dataTask(with: request) {data, response, error in
            
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                if let response = response as? HTTPURLResponse , 200...299 ~= response.statusCode {
                    completion?(json, true);
                } else {
                    if let returnData = String(data: data, encoding: .utf8) {
                        
                        print("returnData \(returnData)")
                        
                    }
                    completion?(json, false);
                }
            }
            }.resume();
        
    }

}
