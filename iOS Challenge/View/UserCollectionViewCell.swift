//
//  UserCollectionViewCell.swift
//  iOS Challenge
//
//  Created by Charles Black on 2/7/19.
//  Copyright © 2019 challenge. All rights reserved.
//

import UIKit

class UserCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var secondImage: UIImageView!
    @IBOutlet weak var thirdImage: UIImageView!
    @IBOutlet weak var collectionNameLabel: UILabel!
    @IBOutlet weak var numberItemsLabel: UILabel!
    
}
