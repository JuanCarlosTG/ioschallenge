//
//  Extensions.swift
//  iOS Challenge
//
//  Created by Charles Black on 2/7/19.
//  Copyright © 2019 challenge. All rights reserved.
//

import UIKit

@IBDesignable extension UICollectionViewCell{
    
    @IBInspectable var borderColor: UIColor {
        set{
            layer.borderColor = newValue.cgColor
        }
        
        get{
            return UIColor(cgColor: layer.borderColor!)
        }
    }
    
    @IBInspectable var borderWidth: CGFloat{
        set{
            layer.borderWidth = newValue
        }
        get{
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat{
        
        set{
            layer.cornerRadius = newValue
        }
        get{
            return layer.cornerRadius
        }
    }
}


extension UIImageView {
    
    func downloadFrom(link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit){
        
        guard let url = URL(string: link) else{return}
        downloadFrom(url: url)
    }
    
    
    func downloadFrom(url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit){
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard
                let httpURLResponse = response as? HTTPURLResponse, 200...299 ~= httpURLResponse.statusCode,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else{return}
            
            DispatchQueue.main.sync {
                self.image = image
            }
        }.resume()
        
    }
}

