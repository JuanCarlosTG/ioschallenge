//
//  ItemCollectionViewCell.swift
//  iOS Challenge
//
//  Created by Charles Black on 2/7/19.
//  Copyright © 2019 challenge. All rights reserved.
//

import UIKit

class ItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var levelLineoImage: UIImageView!
    @IBOutlet weak var importedImage: UIImageView!
    @IBOutlet weak var productStateImage: UIImageView!
    @IBOutlet weak var freeShippingImage: UIImageView!
    @IBOutlet weak var mainImage: UIImageView!
    
    @IBOutlet weak var freeShippingImageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var importedImageHeightConstraint: NSLayoutConstraint!
    
    func configureImporteImage(image: String){
        if image == ""{
            importedImageHeightConstraint.isActive = false
        }else{
            importedImage.image = UIImage(named: image)
        }
    }
    
    func configureFreeShippingImage(image: String){
        if image == ""{
            freeShippingImageHeightConstraint.isActive = false
        }else{
            freeShippingImage.image = UIImage(named: image)
        }
    }
    

}
