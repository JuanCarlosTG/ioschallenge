//
//  ViewController.swift
//  iOS Challenge
//
//  Created by Charles Black on 2/5/19.
//  Copyright © 2019 challenge. All rights reserved.
//

import UIKit

class ChallengeViewController: UIViewController {

    @IBOutlet weak var userCollection: UICollectionView!
    @IBOutlet weak var favouritesCollectionView: UICollectionView!
    @IBOutlet weak var favouritesLabel: UILabel!
    
    var viewModel: ItemsCollectionViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "FAVORITOS"
        self.navigationController?.navigationBar.shadowImage = UIImage()
        viewModel = ItemsCollectionViewModel()
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateViews),
                                               name: NSNotification.Name("itemsUpdated"), object: nil)

    }
    
    @objc func updateViews(){
        
        userCollection.reloadData()
        favouritesCollectionView.reloadData()
        favouritesLabel.text = "Todos mis favoritos (\(viewModel.itemsCollection.count))"
        
    }
    
    func configureUserCollectionCell(cell: UserCollectionViewCell, row: Int) {
        
        cell.collectionNameLabel.text = viewModel.usersCollection[row].nameCollection
        cell.numberItemsLabel.text = "\(viewModel.usersCollection[row].products.count)"
        
        for i in 0 ..< viewModel.usersCollection[row].products.count{
            
            if i > 2{
                break
            }
            
            let image = viewModel.usersCollection[row].products[i].imageURL
            if i == 0 { cell.mainImage.downloadFrom(link: image)}
            else if i == 1 { cell.secondImage.downloadFrom(link: image)}
            else {cell.thirdImage.downloadFrom(link: image)}
            
        }
        
        
    }
    
    func configureItemCell(cell: ItemCollectionViewCell, row: Int) {
    
        let i = viewModel.itemsCollection[row].imageURL
        cell.mainImage.downloadFrom(link: i)
        
        let linioPlusImage      = viewModel.itemsCollection[row].linioPlusLevel == 1 ? "ndIc30PlusSquare" : "ndIc30Plus48Square"
        let conditionTypeImgae  = viewModel.itemsCollection[row].conditionType == "refurbished" ? "ndIc30RefurbishedSquare" : "ndIc30NewSquare"
        let importedImage       = viewModel.itemsCollection[row].isImported ? "ndIc30InternationalSquare" : ""
        let freeShippingImage   = viewModel.itemsCollection[row].freeShipping ? "ndIc30FreeShippingSquare" : ""
        
        cell.levelLineoImage.image = UIImage(named: linioPlusImage)
        cell.productStateImage.image = UIImage(named: conditionTypeImgae)
        cell.configureImporteImage(image: importedImage)
        cell.configureFreeShippingImage(image: freeShippingImage)
       
        
    }
    
    
   

}

extension ChallengeViewController: UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == userCollection{
        
            return viewModel.usersCollection.count
            
        }else{
            
            return viewModel.itemsCollection.count
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == userCollection{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "user_cell", for: indexPath) as! UserCollectionViewCell
            configureUserCollectionCell(cell: cell, row: indexPath.row)
            return cell
            
        }else{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "item_cell", for: indexPath) as! ItemCollectionViewCell
            configureItemCell(cell: cell, row: indexPath.row)
            return cell
            
        }
        
    }
    
}

extension ChallengeViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = userCollection.bounds.size.width/2 - 8
        return CGSize(width: width, height: width)
        
    }
    
}
