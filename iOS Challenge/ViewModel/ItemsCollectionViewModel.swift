//
//  ItemsCollectionViewModel.swift
//  iOS Challenge
//
//  Created by Charles Black on 2/7/19.
//  Copyright © 2019 challenge. All rights reserved.
//

import UIKit

class ItemsCollectionViewModel {
    
    var usersCollection: [UserCollectionModel]
    var itemsCollection: [ItemCollectionModel]
    
    init() {
        
        self.usersCollection = []
        self.itemsCollection = []
        
        CustomServices.call(ws: "https://gist.githubusercontent.com/egteja/98ad43f47d40b0868d8a954385b5f83a/raw/5c00958f81f81d6ba0bb1b1469c905270e8cdfed/wishlist.json") { (data, completed) in
            
            OperationQueue.main.addOperation {
                
                if (!completed) {
                    return
                    
                }
                
                if let collections = data as? [[String:AnyObject]] {
                    
                    var reusableItemCollectionModel: ItemCollectionModel
                    
                    for c in collections{
                        
                        let name = c["name"] as! String
                        let products = c["products"] as! [String: AnyObject]
                        let user = UserCollectionModel(nameCollection: name)
                        
                        for (key, array) in products{
                            
                            if let dataCollection = array as? [String: AnyObject]{
                                
                                reusableItemCollectionModel = self.createItemCollection(key: key, dataCollection: dataCollection)
                                
                                self.itemsCollection.append(reusableItemCollectionModel)
                                user.products.append(reusableItemCollectionModel)
                            
                            }
                            
                        }
                        
                        self.usersCollection.append(user)
                        
                    }
                    
                }
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "itemsUpdated"), object: nil)
                
            }
            
            
            
        }
        
        
    }
    
    
    func createItemCollection(key: String, dataCollection: [String : AnyObject]) -> ItemCollectionModel {
        
        let id = "\(dataCollection["id"] as! Int)"
        let name = dataCollection["name"] as! String
        let image = dataCollection["image"] as! String
        let linioPlusLevel = dataCollection["linioPlusLevel"] as! Int
        let conditionType = dataCollection["conditionType"] as! String
        let freeShipping = dataCollection["freeShipping"] as! Bool
        let imported = dataCollection["imported"] as! Bool
        let active = dataCollection["active"] as! Bool
        
        return ItemCollectionModel(uuid: key, itemID: id, name: name, imageURL: image, linioPlusLevel: linioPlusLevel, conditionType: conditionType, freeShipping: freeShipping, isImported: imported, isActie: active)
    }

}
